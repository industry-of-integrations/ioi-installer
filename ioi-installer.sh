#!/bin/bash

INST_VERSION='2.1.0'

pause() {
	read -n 1 -p "Press [KEY] to continue ..."
	echo
}

usage() {
	echo "usage: $0 [u|q]{i|o|l} PKG[@version] [--registry REGISTRY] [--version VERSION] [--data] [-h|--help]"
	echo "usage: $0 ui --everything [--data]"
	echo
}

help() {
	echo "$0: IOI installer version "$INST_VERSION
	echo
	usage
	echo " Prefix:"
	echo -e "     u  <PKG>\t un{install|load}"
	echo -e "     q  <PKG>\t query"
	echo
	echo " Commands:"
	echo -e "     i  <PKG>\t Install a package; if an agent has a start script, it will be run."
	echo -e "     o  <PKG>\t Operate on an outdated package."
	echo -e "     l  <PKG>\t If the package is an agent, it will loaded."
	echo
	echo " Switches:"
	echo -e "     --registry <REGISTRY>"
	echo -e "    \t Specifies the registry where to find the package; currently UNUSED."
	echo
	echo -e "     --version <VERSION>"
	echo -e "    \t Install a specific version; VERSION is not prefix with a 'v'. @version has precedence."
	echo
	echo -e "     --data"
	echo -e "    \t Also remove all user data associated with the package."
	echo
	echo -e "     --everything"
	echo -e "    \t Instead of uninstalling a single package, uninstall every package found."
	echo -e "    \t Essentially, remove IOI from the system."
	echo
	echo " General:"
	echo -e "     There is no space between a prefix and a command."
	echo
	echo " Examples:"
	echo -e "       To query if a package is installed."
	echo -e "    \t $0 qi @ioi-ri/default-runtime-manager@0.1.2"
	echo -e "    \t $0 qi @ioi-ri/default-runtime-manager --version 0.1.2"
	echo
}

error_msg() {
	echo $0": $1" 1>&2
	echo "FAIL!"
	exit 255
}
error_exit() {
	echo "FAIL!"
	exit 255
}

#--------------------------------------------------------------------------------------------------------------------------------

# set -x

# https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script
#if [[ "$OSTYPE" == "linux-gnu" ]]; then

#TODO: use /proc/version or uname -v instead to find linux version?
# https://en.wikipedia.org/wiki/Uname
#

declare -r MACOS="macOS"
declare -r LAUNCHD="ioi-agent"

UNAME="$(uname -s)"
case "${UNAME}" in
	Linux*)		_OS=linux;;
	Darwin*)	_OS=$MACOS;;
#	CYGWIN*)	_OS=Cygwin;;
#	MINGW*)		_OS=MinGw;;
	*)			error_msg "unsupported operating system: [$UNAME]"
esac

UNAME="$(uname -m)"
case "${UNAME}" in
	arm64)		_ARCH=$UNAME;;
	x86_64)		_ARCH=$UNAME;;
	*)			error_msg "unsupported architecture: [$UNAME]"
esac

# http://linuxcommand.org/lc3_wss0120.php
#
_INST=0
_QUERY_INST=0
_UNINST=0
_LOAD=0
_QUERY_LOAD=0
_UNLOAD=0
_QUERY_OUTDATED=0
_EVERYTHING=0
_PKG=""
_VERSION=""

case $1 in
	i )						_INST=1
							_LOAD=1
							shift
							_PKG=$1
							;;
	ui )					_UNINST=1
							_UNLOAD=1
							shift
							_PKG=$1
							[ "$_PKG" = "--everything" ] && _EVERYTHING=1
							;;
	qo )					_QUERY_OUTDATED=1
							shift
							_PKG=$1
							;;
	qi )					_QUERY_INST=1
							shift
							_PKG=$1
							;;
	l )						_LOAD=1
							shift
							_PKG=$1
							;;
	ul )					_UNLOAD=1
							shift
							_PKG=$1
							;;
	ql )					_QUERY_LOAD=1	
							shift
							_PKG=$1
							;;
	-h | --help )			help
							exit 0
							;;
	* )						usage
							exit 255
esac
shift

_REG='npm://registry.npmjs.org'
_DATA=0
_DEV=0

while [ ! -z "$1" ]; do
	case $1 in
		--registry )			shift
								_REG=$1
								;;
		--version )				shift
								_VERSION=$1
								;;
		--data )				_DATA=1
								;;
		--dev )					_DEV=1
								;;
		-h | --help )			help
								exit 0
								;;
		* )						usage
								exit 255
	esac
	shift
done

[ -z "$_PKG" ] && { usage && error_exit; }
if (( ! $_UNINST )); then
	[ "$_PKG" = "--everything" ] && { usage && error_exit; }
	[ "$_PKG" = "--data" ] && { usage && error_exit; }
fi

#echo $_PKG | sed -E 's/^(((@[a-z_-]*)\/)?([a-z_-]*))(@([0-9.]*))?$/[\1][\2][\3][\4][\5][\6]/'
PKG_SCOPE=`echo $_PKG | sed -E 's/^(((@[a-z_-]*)\/)?([a-z_-]*))(@([a-z0-9.-]*))?$/\3/' `
#echo $PKG_SCOPE
PKG_PKG=`echo $_PKG | sed -E 's/^(((@[a-z_-]*)\/)?([a-z_-]*))(@([a-z0-9.-]*))?$/\1/' `
#echo $PKG_PKG
PKG_VERSION=`echo $_PKG | sed -E 's/^((@[a-z_-]*)\/)?([a-z_-]*)(@([a-z0-9.-]*))?$/\5/' ` 
if [ -z "$PKG_VERSION" ]; then
	PKG_VERSION=$_VERSION
fi
#echo $PKG_VERSION

REG_TYPE=`echo $_REG | sed -E 's/^([[:alpha:]]+:\/\/)?.*$/\1/' `
#echo $REG_TYPE

#--------------------------------------------------------------------------------------------------------------------------------

cleanse_str()
{
	SRC="$1"
	echo ${SRC/./_}
}
# https://www.shell-tips.com/bash/functions/#gsc.tab=0
# usage: RESULT=$(cleanse_str "$X") # no space after = sign!

#--------------------------------------------------------------------------------------------------------------------------------

# https://stackoverflow.com/a/589210/298545 // cannot use: cd ~"/My Code"; use $HOME instead
LIBRARY="$HOME/Library"
LAUNCH_AGENTS="$LIBRARY/LaunchAgents"

#APP_SUPPORAPP_SUPPORTT="$LIBRARY/Application Support"
IOI_DATA="$LIBRARY/IOI"

IOI="$HOME/.ioi"
IOI_BIN="$IOI/bin"
IOI_CACHE="$IOI/~cache"

#IOI_PKG="$IOI/$(cleanse_str "$_PKG")"
#IOI_DATA_PKG="$IOI_DATA/$(cleanse_str "$_PKG")"
IOI_PKG="$IOI/node_modules/$PKG_PKG"
[ "$REG_TYPE" = "depot://" ] && IOI_PKG="$IOI/$PKG_PKG"
IOI_DATA_PKG="$IOI_DATA/$PKG_PKG"

NODEJS="$IOI/node"
PATH=$PATH:"$NODEJS/bin"

#--------------------------------------------------------------------------------------------------------------------------------

check_installer_version()
{
	# (23) Failed writing body: 
	# https://stackoverflow.com/questions/16703647/why-does-curl-return-error-23-failed-writing-body
	# This happens when a piped program (e.g. grep) closes the read pipe before the previous program is finished writing the whole page.
	# ... as soon as grep has what it wants it will close the read stream from curl.
	# 
	NET_INST_URL=https://raw.githubusercontent.com/knev/mim-installer/master/install.sh
	NET_INST_VERSION=`curl -sfL --url $NET_INST_URL 2>/dev/null | sed -nE '/INST_VERSION=[0-9]+/p' | sed 's/^INST_VERSION=\(.*\)$/\1/'  `

	if [ -z "$NET_INST_VERSION" ]; then
		echo "Unable to check for installer updates, currently [v$INST_VERSION]"
		read -s -n 1 -p "Please check manually for a newer version, continue? [N/y] " INPUT || error_exit
		RES=$( tr '[:upper:]' '[:lower:]' <<<"$INPUT" )
		if [[ "$RES" != "y" ]]; then
			echo
			echo "Abort."
			exit 0
		fi
		echo
	else
		[ "$INST_VERSION" -lt "$NET_INST_VERSION" ] && error_msg "This installer is outdated [v$INST_VERSION]. Please obtain the newer [v$NET_INST_VERSION]."
	fi
}

install_npm()
{
	which 'node' > /dev/null && which 'npm' > /dev/null && return 0;

	NODE_INST_URL=''
	if [ "$_OS" = 'macOS' ]; then
		if [ "$_ARCH" = 'x86_64' ]; then
			NODE_INST_URL=https://nodejs.org/dist/v18.16.0/node-v18.16.0-darwin-x64.tar.gz
		elif [ "$_ARCH" = 'arm64' ]; then
			NODE_INST_URL=https://nodejs.org/dist/v18.16.0/node-v18.16.0-darwin-arm64.tar.gz
		fi
	fi

	if [ -z "$NODE_INST_URL" ]; then 
		echo $0": Unable to find corresponding node/npm binaries for [$_OS,$_ARCH]" 1>&2
		return 0; # if node/npm isn't installed, check_pre_reqs will FAIL!
	fi
	mkdir -p "$IOI_CACHE"
	NODE_FILENAME=`basename $NODE_INST_URL`

	echo "Downloading: $NODE_INST_URL"
	# If you'd like to turn off curl's verification of the certificate, use the -k (or --insecure) option.
	curl -k -f -o "$IOI_CACHE"/$NODE_FILENAME -L $NODE_INST_URL || error_exit
	tar xzf "$IOI_CACHE"/$NODE_FILENAME -C "$IOI"
	#TGZ_TOP_LEVEL="${NODE_FILENAME%.tar.gz}"
	TGZ_TOP_LEVEL=`tar -tf "$IOI_CACHE"/$NODE_FILENAME | head -1 | cut -d '/' -f 1`
	echo "$IOI"/$TGZ_TOP_LEVEL $NODEJS
	ln -sfn "$IOI"/$TGZ_TOP_LEVEL $NODEJS

	rm "$IOI_CACHE"/$NODE_FILENAME 
}

check_pre_reqs()
{
	REQ=node; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
	REQ=npm; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
	REQ=curl; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
	REQ=tar; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
	REQ=xmllint; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	REQ=g++; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	REQ=git; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	REQ=automake; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	REQ=make; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	REQ=pkg-config
#	REQ=autoconf
#	REQ=curl; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	REQ=unzip; which $REQ > /dev/null || error_msg "This install script requires [$REQ] to be installed"
#	
#	#TODO host:/usr/local/opt/zmq$ [ -f include/zmq.h ] || echo "NO"
#	if [[ $_OS = macOS ]]; then
#		REQ=zmq # ./configure will fail if not installed
#	elif [[ $_OS = linux ]]; then
#		REQ=libzmq3-dev # ./configure will fail if not installed
#	fi
}

#--------------------------------------------------------------------------------------------------------------------------------

install_pkg() 
{

#	if (( ! $CLEAN )) && [ -z $REQ_VERSION ]; then
#		if [ -f ./mim-$SIDE'stream.jar' ]; then
#			JAR=( `echo $JAR_VERSION | sed 's/^v\([0-9]*\)\.\([0-9]*\)-\([0-9]*\)$/\1 \2 \3/'` )
#			NET=( `echo $NET_VERSION | sed 's/^v\([0-9]*\)\.\([0-9]*\)-\([0-9]*\)$/\1 \2 \3/'` )
#			UPGRADE=0; for NR in 0 1 2; do [ ${JAR[$NR]} -lt ${NET[$NR]} ] && UPGRADE=1; done
#
#			(( ! $UPGRADE )) && { echo "Download MiM-"$SIDE"stream component :: SKIPPED"; return 0; }
#
#			echo "MiM-"$SIDE"stream-v"${JAR[0]}.${JAR[1]}-${JAR[2]}" installed, latest [v"${NET[0]}.${NET[1]}-${NET[2]}"], upgrading ..."
#		fi
#	fi

	# -----

	npm --prefix $IOI --install-strategy=shallow install $PKG_PKG'@'$PKG_VERSION
}

#--------------------------------------------------------------------------------------------------------------------------------

load_pkg()
{
	echo "loading $PKG_PKG"
	npm --prefix $IOI_PKG start
}

unload_pkg()
{
	echo "unloading $PKG_PKG"
	npm --prefix $IOI_PKG stop
}

query_load_pkg()
{
	echo "querying load $PKG_PKG"
	npm --prefix $IOI_PKG test
}

#--------------------------------------------------------------------------------------------------------------------------------

query_install_pkg()
{
	npm --prefix $IOI --json ls $PKG_PKG'@'$PKG_VERSION
}

un_install_pkg()
{
	# trying to uninstall a PKG with a version, does not work!
	echo "Uninstalling: $PKG_PKG"
	npm --prefix $IOI_PKG run uninstall
	npm --prefix $IOI uninstall $PKG_PKG || return 255;
	(( $_DATA )) && { echo; echo "deleting ..." && rm -rfv "$IOI_DATA_PKG" 2>/dev/null; }

	return 0;
}


#--------------------------------------------------------------------------------------------------------------------------------

query_outdated_pkg()
{
	NPM_LS=`npm --prefix $IOI -parseable --long ls $PKG_PKG`
	LS_LOCATION=`echo $NPM_LS | cut -d: -f1`
	LS_VERSION=`echo $NPM_LS | cut -d: -f2 | sed -E 's/^((@[a-z_-]*)\/)?([a-z_-]*)(@([a-z0-9.-]*))?$/\5/' ` 

	#NOTE: the return value for `npm outdated` is 
	#	0: nothing needs updating
	#	1 && output : a package needs updating
	#	1 && no output: program error

	# no version number after PKG for outdated
	NPM_OUTDATED_JSON=`npm --prefix $IOI --json outdated $PKG_PKG`

	if [ "$NPM_OUTDATED_JSON" = "{}" ] && [ -n "$NPM_LS" ]; 
	then
		#[ -n "$NPM_LS" ] && 

cat << EOF 
{
  "$PKG_PKG": {
    "current": "$LS_VERSION",
    "wanted": "$LS_VERSION",
    "latest": "$LS_VERSION",
    "location": "$LS_LOCATION"
  }
}
EOF

	else 
		echo $NPM_OUTDATED_JSON
	fi


#	[ -d "$IOI_DATA_PKG" ] && echo "DATA: $IOI_DATA_PKG"
#
#	echo "PKG: $IOI_PKG @v$PKG_VERSION"
#	# get_plist_info is going to fail before this does; the Info.plist must exist
#	[ ! -d "$IOI_PKG" ] && return 255
#
#	if [ "$IOI_TYPE" = "$IOI_LAUNCHDAGENT" ]; then 
#		echo "PLIST: $BUNDLE_ID.plist in $LAUNCH_AGENTS/."
#	fi
#
#	if [ "$IOI_TYPE" = "$IOI_COMMANDLINEBINARY" ]; then 
#		echo "SYMLINK: $IOI_BIN/$_PKG"
#		[ ! -L "$IOI_BIN/$_PKG" ] && return 255
#	fi

	return 0;
}

#--------------------------------------------------------------------------------------------------------------------------------

process_pkg()
{
	if (( $_INST )); then
		#check_latest_version || error_exit
		install_pkg || error_exit 
	fi

	(( $_LOAD )) && { load_pkg || error_exit; }
	(( $_QUERY_LOAD )) && { query_load_pkg || error_exit; }
	(( $_UNLOAD )) && { unload_pkg || error_exit; }

	(( $_QUERY_INST )) && { query_install_pkg || error_exit; }
	(( $_QUERY_OUTDATED )) && { query_outdated_pkg || error_exit; }
	(( $_UNINST )) && { un_install_pkg || error_exit; }

	return 0;
}

#--------------------------------------------------------------------------------------------------------------------------------
#################################################################################################################################
#--------------------------------------------------------------------------------------------------------------------------------

NET_VERSION=""
NET_DOWNLOAD=`echo $_REG | sed -E 's/depot:\/\//https:\/\//'`"/$PKG_PKG" # curl has -L switch, so should be ok to leave off the www

check_latest_version()
{
	if [ -z "$_VERSION" ]; then
		# we are grepping 2 lines, so assign to an array.
		NET_VERSION=`curl -sfL $NET_DOWNLOAD/Info.plist | grep -A 1 CFBundleShortVersionString | tail -n 1 | sed 's/^.*<string>\(.*\)<\/string>$/\1/'`
		# NET_VERSION=`echo $NET_LONG_VERSION | sed -nE '/^v[0-9]+.[0-9]+-[0-9]+-.*$/p' | sed 's/^\(v[0-9]*\.[0-9]*-[0-9]*\)-.*$/\1/'`
		# echo $NET_VERSION
	else
		NET_VERSION=$_VERSION
		echo ! Requested version manually set to [$NET_VERSION]
	fi

	[ -z "$NET_VERSION" ] && error_msg "Unable to determine the latest version of [$_PKG]"

	return 0; # return value of the echo command is the number of arguments that were printed to the console
}

#--------------------------------------------------------------------------------------------------------------------------------

depot_install_pkg()
{
	PKGV=$_PKG'@'$NET_VERSION
	EXT='.tar.gz'
	echo "== Downloading ["$PKGV"] component =="
	mkdir -p "$IOI_CACHE"
	#echo $NET_DOWNLOAD/$PKGV$EXT
	curl -f -o "$IOI_CACHE"/$PKGV$EXT -L $NET_DOWNLOAD/$PKGV$EXT || return 1
	#[ -f "$IOI"/$PKG$EXT ] && { mv "$IOI"/$PKG$EXT "$IOI"/$PKG$EXT'~' || return 1; }
	
#	if [ "$_SHA2" = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" ]; then
#		echo "shasum: WARNING: sha checksum of [$PKGV$EXT] can not be verified!"
#	else
#		echo "$_SHA2 *$IOI_CACHE/$PKGV$EXT" | shasum -b -a 384 -c
#		[ $? -ne 0 ] && return 255
#	fi

	mkdir -p "$IOI_PKG"
	tar -xzvf "$IOI_CACHE"/$PKGV$EXT --directory "$IOI_PKG"
	rm "$IOI_CACHE"/$PKGV$EXT

	echo $_PKG': v'$NET_VERSION' -> '"$IOI_PKG"
	return 0;
}

depot_un_install_pkg()
{
	# trying to uninstall a PKG with a version, does not work!
	echo "Uninstalling: $PKG_PKG"

	# echo "=== [$LAUNCHD]"
	bash $IOI_PKG/bin/$LAUNCHD d $_PKG --depot 2>/dev/null
	ERR=$?
	# echo "[$LAUNCHD] ==="

	rm -rfv "$IOI/$_PKG" 2>/dev/null
	(( $_DATA )) && { echo "deleting ..." && rm -rfv "$IOI_DATA_PKG" 2>/dev/null; }

	return 0;
}

depot_query_outdated_pkg()
{
	[[ ! -e "$IOI_PKG" ]] && { echo "{}"; return 0; }

	LS_VERSION=`xmllint --xpath "/plist/dict/key[text()='CFBundleShortVersionString']/following-sibling::string[1]/text()" "$IOI_PKG"/Info.plist 2> /dev/null `
	LS_LOCATION="$IOI_PKG"

cat << EOF 
{
  "$PKG_PKG": {
    "current": "$LS_VERSION",
    "wanted": "$LS_VERSION",
    "latest": "$LS_VERSION",
    "location": "$LS_LOCATION"
  }
}
EOF

	return 0;
}

#--------------------------------------------------------------------------------------------------------------------------------

#	declare -r IOI_LAUNCHDAGENT="IOI_LaunchdAgent"
#	declare -r IOI_COMMANDLINEBINARY="IOI_CommandLineBinary"
#	
#	BUNDLE_ID="id.bundle.invalid"
#	PKG_PLIST="$LAUNCH_AGENTS/$BUNDLE_ID.plist"
#	PKG_VERSION="#.#.#"
#	IOI_TYPE="IOI_TYPE_Invalid"
#	IOI_PLIST_DICT=""
#	
#	get_plist_info()
#	{
#		BUNDLE_ID=`xmllint --xpath "/plist/dict/key[text()='CFBundleIdentifier']/following-sibling::string[1]/text()" "$IOI_PKG"/Info.plist 2> /dev/null `
#		[ -z $BUNDLE_ID ] && return 255
#	
#		PKG_PLIST="$LAUNCH_AGENTS/$BUNDLE_ID.plist"
#		PKG_VERSION=`xmllint --xpath "/plist/dict/key[text()='CFBundleShortVersionString']/following-sibling::string[1]/text()" "$IOI_PKG"/Info.plist 2> /dev/null `
#	
#		for IOI_KEY in $IOI_LAUNCHDAGENT $IOI_COMMANDLINEBINARY
#		do
#			KEY_EXISTS=`xmllint --xpath "count(/plist/dict/key[text()='$IOI_KEY'])" "$IOI_PKG"/Info.plist `
#			if (( $KEY_EXISTS )); then
#				[ $IOI_TYPE != "IOI_TYPE_Invalid" ] && return 255
#				IOI_TYPE=$IOI_KEY
#				IOI_PLIST_DICT=`xmllint --xpath "/plist/dict/key[text()='$IOI_TYPE']/following-sibling::dict[1]" "$IOI_PKG"/Info.plist 2> /dev/null `
#			fi
#		done
#		
#		[ -z $IOI_TYPE ] && return 255
#	
#		return 0;
#	}

#--------------------------------------------------------------------------------------------------------------------------------

write_launchd_plist()
{
	# echo "=== [$LAUNCHD]"
	bash $IOI_PKG/bin/$LAUNCHD w $_PKG --depot 2>/dev/null
	ERR=$?
	# echo "[$LAUNCHD] ==="
	return $ERR
}

#--------------------------------------------------------------------------------------------------------------------------------

launchd_load()
{
	# echo "=== [$LAUNCHD]"
	bash $IOI_PKG/bin/$LAUNCHD l $_PKG --depot 2>/dev/null
	ERR=$?
	# echo "[$LAUNCHD] ==="
	return $ERR
}
	
launchd_query() 
{
	# echo "=== [$LAUNCHD]"
	bash $IOI_PKG/bin/$LAUNCHD ql $_PKG --depot 2>/dev/null
	ERR=$?
	# echo "[$LAUNCHD] ==="
	return $ERR
}

launchd_unload()
{
	# echo "=== [$LAUNCHD]"
	bash $IOI_PKG/bin/$LAUNCHD ul $_PKG --depot 2>/dev/null
	ERR=$?
	# echo "[$LAUNCHD] ==="
	return $ERR
}

#--------------------------------------------------------------------------------------------------------------------------------

depot_process_pkg()
{
	if (( $_INST )); then
		check_latest_version || error_exit;
		depot_install_pkg || error_exit 
	fi

	#get_plist_info || error_exit;

#	if [ "$IOI_TYPE" = "$IOI_LAUNCHDAGENT" ]; then 
		(( $_INST )) && { write_launchd_plist || error_exit; }
		(( $_LOAD )) && { launchd_load || error_exit; }
		(( $_QUERY_LOAD )) && { launchd_query || error_exit; }
		(( $_UNLOAD )) && launchd_unload 
#	fi
#
#
#	if [ "$IOI_TYPE" = "$IOI_COMMANDLINEBINARY" ]; then 
#		if (( $_INST )); then
#			bin_symlink || error_exit
#		fi
#		if (( $_UNINST )); then
#			bin_unlink || error_exit
#		fi
#
#	fi
#
	(( $_QUERY_OUTDATED )) && { depot_query_outdated_pkg || error_exit; }
	(( $_UNINST )) && { depot_un_install_pkg || error_exit; }

	return 0;
}

#--------------------------------------------------------------------------------------------------------------------------------

#	bin_symlink() {
#		mkdir -p "$IOI_BIN"
#		ln -sfv  "$IOI_PKG/$_PKG" "$IOI_BIN/$_PKG"
#		return 0;
#	}
#	
#	bin_unlink() {
#		unlink "$IOI_BIN/$_PKG"
#		return 0;
#	}

#--------------------------------------------------------------------------------------------------------------------------------
#################################################################################################################################
#--------------------------------------------------------------------------------------------------------------------------------

if [ "$REG_TYPE" = "npm://" ]; then

#	check_installer_version
	install_npm || error_exit
	check_pre_reqs || error_exit

	if (( $_EVERYTHING )); then 

		# https://stackoverflow.com/questions/26769493/how-do-i-loop-through-a-directory-path-stored-in-a-variable
		# 	if you add the trailing slash, the DIR entries will also have a trailing slash
#		for DIR in "$IOI"/*
#		do
#			[ ! -d "$DIR" ] && continue;
#
#			[ "$DIR" = "$IOI_BIN" ] && continue
#			[ "$DIR" = "$IOI_CACHE" ] && rm -rfv "$DIR" 2>/dev/null && continue
#
#			BUNDLE_NAME=`xmllint --xpath "/plist/dict/key[text()='CFBundleName']/following-sibling::string[1]/text()" "$DIR"/Info.plist 2> /dev/null `
#			[ -z $BUNDLE_NAME ] && return 255
#			_PKG=$BUNDLE_NAME
#
#			MAIN_NIB_FILE=`xmllint --xpath "count(/plist/dict/key[text()='NSMainNibFile'])" "$DIR"/Info.plist 2> /dev/null `
#			(( $MAIN_NIB_FILE )) && _PKG+='.app'
#
#			IOI_PKG="$IOI/$(cleanse_str "$_PKG")"
#			IOI_DATA_PKG="$IOI_DATA/$(cleanse_str "$_PKG")"
#
#			process_pkg
#			echo "-"
#
#		done
#
#		rm -v "$IOI_BIN/ioi-installer" 2>/dev/null
#		rmdir -v "$IOI_BIN" 2>/dev/null
#
#		[ "$_OS" = "$MACOS" ] && rm -v "$IOI/.DS_Store" 2>/dev/null
#		(( $_DATA )) && rmdir -v "$IOI_DATA" 2>/dev/null
#		rmdir -v "$IOI" 2>/dev/null
		echo "Not Impld!" && error_exit;

	else
		process_pkg

	fi

#	(( ! $_DEV )) && {   }
#	(( $_DEV )) && exit 0

elif [ "$REG_TYPE" = "depot://" ]; then

	depot_process_pkg

else
	usage
	exit 255
fi

#--------------------------------------------------------------------------------------------------------------------------------

echo "SUCCESS!"; exit 0
